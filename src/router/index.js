import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home/HomeContainer'
import Login from '@/components/auth/LoginContainer'
import Admin from '@/components/admin/AdminContainer'
import Fleet from '@/components/fleet/FleetContainer'
import Reports from '@/components/fleet/reports/ReportsMain'
import VehiclesTable from '@/components/fleet/vehicles/VehiclesTable'
import LocationsTable from '@/components/fleet/locations/LocationsTable'
import NewLocation from '@/components/fleet/locations/NewLocation'
import TechSupport from '@/components/tech-support/TechSupportContainer'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin
    },
    {
      path: '/fleet',
      component: Fleet,
      children: [
        {
          path: 'locations',
          component: LocationsTable
        },
        {
          path: 'locations/new',
          component: NewLocation
        },
        { path: 'reports', component: Reports },
        { path: 'locations/:locationId/vehicles', component: VehiclesTable }
      ]
    },
    {
      path: '/tech-support',
      name: 'TechSupport',
      component: TechSupport
    }
  ]
})
