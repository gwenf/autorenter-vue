import axios from 'axios'

export default {
  login: ({commit}, credentials) => {
    // 'https://autorenter-nodeexpress-api.herokuapp.com/api/login'
    axios.post(
      'http://localhost:3000/api/login',
      { username: credentials.username, password: credentials.password }
    )
    .then(({ data }) => {
      localStorage.setItem('username', data.username)
      localStorage.setItem('token', data.token)
      return data
    })
    .then((userData) => {
      // return dispatch(loggedInSuccess(userData))
      console.log(userData)
      commit('setUserCredentials', userData)
    })
    .catch((err) => {
      console.log(err)
    })
  },
  logout: ({commit}) => {
    localStorage.removeItem('username')
    localStorage.removeItem('token')

    commit('setUserCredentials', null)
  },
  fetchLocations: ({commit}) => {
    const url = `${process.env.API_URL}locations/`
    const token = localStorage.getItem('token')

    axios.get(url, { headers: { Authorization: token } }).then(
      (res) => {
        console.log(res.data)
        const locations = res.data.locations
        commit('setLocations', locations)
      }).catch(
        (axiosError) => {
          console.log(axiosError)
          actionMethods.handleErrror(axiosError, types.GET_LOCATIONS_ERROR, dispatch)
        }
      )
  }
}
