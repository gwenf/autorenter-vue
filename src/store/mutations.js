export default {
  setUserCredentials: (state, user) => {
    state.user = user
  },
  setLocations: (state, locations) => {
    state.locations = locations
  }
}
