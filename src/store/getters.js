export default {
  user: (state) => {
    return state.user
  },
  locations: (state) => {
    return state.locations
  }
}
