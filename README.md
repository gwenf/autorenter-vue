# Autorenter Vue

This project has been bootstrapped with vue-cli.

[Autorenter Specification](https://github.com/fusionalliance/autorenter-spec)

### Technologies

1. Vue
1. Vue-Router
1. VueX
1. Foundation-sites
1. Sass
1. Ion Icons
1. Connects to AWS Microservices

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Sass

All classes and styling should be contained within `src/assets/sass`. For each component a new styling file is created within the components directory here (eg. `components/_pathway.sass`). Since each of these component files is imported and bundled into one main file, each one should be namespaced to avoid polluting the global scope:

```vue
// Component file
<template>
  <div class="pathway-container">
  ...
  </div>
</template>
```

```sass
// Sass file for component
.pathway-container
  // all other styles inside that component
```

## Style Guide

[Foundation-sites](https://foundation.zurb.com/sites.html) is being used as a styling framework. Other styling considerations like the default spacing and page width are coming soon. X-grid is the specific grid layout being used with Foundation.

## Icons

[Ion Icons](http://ionicons.com/) are being used exclusively right now for this project. They are open source and MIT licensed.

## Testing

Unit testing is important :) Testing goals will be ~50% coverage for components to start with and closer to 100% for state management.

The plan is to use vue-test-utils with the Jest testing framework.

## Linting

The default vue-cli .eslint.js setup is being used right now with a few rule modifications.
